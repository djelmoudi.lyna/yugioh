package Controller;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import API.API;
import card.card;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
public class searchCardEngineController {
	
	@FXML 
	ListView cardListView;
	
	public void ShowListCard()
	{
		String cardList = API.searchCard();
		
		//((Object) cardList.get("data"))).filter();
		
		//cardListView.setCellFactory(value);
		// convert JSON array to List
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			List<card> list = Arrays.asList(mapper.readValue(cardList, card[].class));
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
